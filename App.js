import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Index from './src/index'
import firebase from "firebase/app"


const firebaseConfig = {
  apiKey: "AIzaSyAs-K6FO99cK9msBkvrihBzN1tyQ-s7hlc",
  authDomain: "todo-app-d7341.firebaseapp.com",
  projectId: "todo-app-d7341",
  storageBucket: "todo-app-d7341.appspot.com",
  messagingSenderId: "459784859091",
  appId: "1:459784859091:web:473caa86e0c9af01d0d326"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export default function App() {
  return (
    <Index />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
