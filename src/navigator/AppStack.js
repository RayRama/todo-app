import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "../screens/HomeScreen";
import AboutScreen from "../screens/AboutScreen";
import SettingsScreen from "../screens/SettingsScreen";
import SkillScreen from "../screens/SkillScreen";
import InfoApp from "../screens/InfoApp";
import { Ionicons } from "@expo/vector-icons";
import "react-native-gesture-handler";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Main"
      options={{ headerShown: false }}
      screenOptions={{
        presentation: "modal",
        animationTypeForReplace: "pop",
      }}
    >
      <Stack.Screen
        name="Main"
        component={MainApp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="About Us"
        component={AboutScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Skill"
        component={SkillScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Info"
        component={InfoApp}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const MainApp = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      headerShown: false,
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === "Home") {
          iconName = focused ? "home" : "home-outline";
        } else if (route.name === "Settings") {
          iconName = focused ? "settings" : "settings-outline";
        }
        return <Ionicons name={iconName} size={size} color={color} />;
      },
      tabBarActiveTintColor: "#85B6FF",
      tabBarInactiveTintColor: "#555555",
    })}
  >
    <Tab.Screen name="Home" component={HomeScreen} />
    <Tab.Screen name="Settings" component={SettingsScreen} />
  </Tab.Navigator>
);

export default AppStack;
