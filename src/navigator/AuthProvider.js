import React, { createContext, useState } from "react";
import firebase from "firebase";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        login: async (email, password) => {
          try {
            await firebase
              .auth()
              .signInWithEmailAndPassword(email, password)
              .then((userCredential) => {
                const uid = userCredential.user.uid;
                const usersRef = firebase.firestore().collection("users");
                usersRef
                  .doc(uid)
                  .get()
                  .then((firestoreDocument) => {
                    if (!firestoreDocument.exists) {
                      alert("User tidak ditemukan");
                      return;
                    }
                  })
                  .catch((err) => {
                    alert(err);
                  });
              });
          } catch (error) {
            alert(error);
          }
        },
        register: async (email, password) => {
          try {
            await firebase
              .auth()
              .createUserWithEmailAndPassword(email, password)
              .then(() => {
                firebase
                  .firestore()
                  .collection("users")
                  .doc(firebase.auth().currentUser.uid)
                  .set({
                    email: email,
                    createdAt: firebase.firestore.Timestamp.fromDate(
                      new Date()
                    ),
                  });
              });
          } catch (error) {
            var errorCode = error.code;
            var errorMessage = error.message;

            if (errorCode == "auth/email-already-in-use") {
              alert("Email sudah ada. Gunakan Email lain !");
            } else if (errorCode == "auth/weak-password") {
              alert("Password terlalu lemah");
            } else {
              alert(errorMessage);
            }
          }
        },
        logout: async () => {
          try {
            await firebase.auth().signOut();
          } catch (error) {
            alert(error);
          }
        },
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
