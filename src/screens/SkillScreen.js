import React, { Component } from "react";
import {
  Text,
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
const { width: Width, height: HEIGHT } = Dimensions.get("window");

export default class SkillScreen extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../asset/profile.png")} />
        <Text style={styles.title}>Ray Ramadita</Text>
        <Text style={styles.subTitle}>React Native Developer</Text>

        <View style={styles.contentBox}>
          <TouchableOpacity style={styles.skillButton}>
            <Text style={styles.skillText}>Daftar Skill</Text>
          </TouchableOpacity>

          <Ionicons
            name="ios-logo-react"
            size={77}
            color="gray"
            style={{ top: 70 }}
          />
          <View style={{ top: 90, width: "100%", left: "10%" }}>
            <Text style={{ fontSize: 20, fontWeight: "300" }}>
              Framework: React Native
            </Text>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Tingakatan: Basic React Native
            </Text>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Tingkat Penguasaan: 55%
            </Text>
          </View>

          <View
            style={{
              top: "20%",
              backgroundColor: "#C4C4C4",
              height: 20,
              width: "80%",
              borderRadius: 7,
            }}
          >
            <View
              style={{
                backgroundColor: "#4299FF",
                height: 20,
                width: "55%",
                borderRadius: 7,
              }}
            ></View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#4299FF",
  },
  logo: {
    width: 105,
    height: 105,
  },
  title: {
    color: "white",
    fontSize: 32,
    fontWeight: "bold",
  },
  subTitle: {
    color: "white",
    fontSize: 14,
    marginBottom: 20,
  },
  contentBox: {
    top: 30,
    width: Width,
    height: "70%",
    backgroundColor: "white",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    alignItems: "center",
  },
  skillButton: {
    width: "50%",
    height: 40,
    borderRadius: 7,
    backgroundColor: "#4299FF",
    top: 30,
  },
  skillText: {
    color: "white",
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
    top: 10,
  },
});
