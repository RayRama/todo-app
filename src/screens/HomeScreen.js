import React, { useState, useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  FlatList,
  Modal,
  ActivityIndicator,
  BackHandler,
  ToastAndroid,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
import colors from "../../Colors";
import tempData from "../../tempData";
import TodoList from "../components/todoList";
import AddListModal from "../components/addListModal";
import { AuthContext } from "../navigator/AuthProvider";
import firebase from "firebase";
import Colors from "../../Colors";
import { onSnapshot } from "../services/colletions";

export default class HomeScreen extends React.Component {
  static contextType = AuthContext;
  state = {
    addModalVisible: false,
    lists: [],
    user: {},
    loading: true,
  };

  toggleAddTodoModal() {
    this.setState({ addModalVisible: !this.state.addModalVisible });
  }

  renderlist = (list) => {
    return (
      <TodoList
        list={list}
        updateList={this.updateList}
        deleteList={this.deleteList}
      />
    );
  };

  addList = (list) => {
    let addListRef = firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("lists");

    addListRef.add(list);
  };

  updateList = (list) => {
    let updateListRef = firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("lists");
    updateListRef.doc(list.id).update(list);
  };

  deleteList = (list) => {
    let deleteListRef = firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("lists");
    deleteListRef.doc(list.id).delete();
  };

  componentDidMount() {
    const { user } = this.context;
    this.setState({ user });

    this.mounted = true;

    const userId = firebase.auth().currentUser.uid;
    const listRef = firebase
      .firestore()
      .collection("users")
      .doc(userId)
      .collection("lists")
      .orderBy("date", "desc");
    onSnapshot(listRef, (newLists) => {
      this.setState({ lists: newLists }, () => {
        this.setState({ loading: false });
      });
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={Colors.blue} />
        </View>
      );
    }

    let date = new Date();
    let hours = date.getHours();

    let greeting = "";
    {
      hours >= 12
        ? hours >= 16
          ? hours >= 18
            ? (greeting = "Malam")
            : (greeting = "Sore")
          : (greeting = "Siang")
        : (greeting = "Pagi");
    }

    // console.log(greeting);
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          visible={this.state.addModalVisible}
          onRequestClose={() => this.toggleAddTodoModal()}
        >
          <AddListModal
            closeModal={() => this.toggleAddTodoModal()}
            addList={this.addList}
          />
        </Modal>

        <Image
          style={styles.profile}
          source={require("../asset/profile.png")}
        />
        <Text style={styles.title}>Selamat {greeting} </Text>
        <Text style={styles.subTitle}>Ingin mengerjakan apa hari ini?</Text>
        <View style={styles.divider}></View>

        <View style={{ top: 60, alignItems: "center", height: "40%" }}>
          {this.state.lists.length > 0 ? (
            <FlatList
              data={this.state.lists}
              keyExtractor={(item) => item.id.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => this.renderlist(item)}
              keyboardShouldPersistTaps={"always"}
            />
          ) : (
            <Text
              style={{
                top: "50%",
                fontWeight: "bold",
                fontSize: 18,
                textAlign: "center",
                paddingRight: 30,
                paddingLeft: 30,
              }}
            >
              ToDo mu masih kosong, silahkan buat ToDo mu
            </Text>
          )}
        </View>

        <View
          style={{ top: 80, alignItems: "center", justifyContent: "center" }}
        >
          <TouchableOpacity onPress={() => this.toggleAddTodoModal()}>
            <AntDesign name="plussquareo" size={40} color="#4299FF" />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ color: "black", fontSize: 24 }}>Add</Text>
            <Text style={{ color: "#4299FF", fontSize: 24 }}>List</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  profile: {
    width: 68,
    height: 68,
  },
  title: {
    top: 10,
    color: "black",
    fontSize: 24,
    fontWeight: "bold",
  },
  subTitle: {
    top: 10,
    color: "black",
    fontSize: 18,
  },
  divider: {
    top: 30,
    width: 100,
    height: 5,
    backgroundColor: "#4299FF",
  },
});
