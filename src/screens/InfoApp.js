import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

export default class InfoApp extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textContent}>
          Tentang Aplikasi:
          {"\n\n"}
          ToDo - App adalah aplikasi untuk membuat list pekerjaan atau kegiatan
          yang ingin dilakukan hari itu ataupun nanti. Aplikasi ini merupakan
          mini project yang yang saya kembangkan sebagai sarana pengembangan
          diri dalam dunia coding juga mengasah kemampuan logika. Aplikasi ini
          menggunakan firebase sebagai back-end untuk menyimpan data secara
          cloud dan juga fungsionalitas login & register di dalam aplikasi.{" "}
        </Text>

        <Text style={styles.textContent}>
          Library:
          {"\n\n"}- Expo {"\n"}- Firebase {"\n"}- React Navigation {"\n"}- Context API State Management{" "}
        </Text>

        <Text style={styles.textContent}>
          Referensi:
          {"\n\n"}- UI Figma Ray Ramadita {"\n"}- Jujutsu Kaisen (Sukuna's Tattoo){"\n"}- Channel Youtube & Website: {"\n"}
          {"    "}- Design Into Code
          {"\n"}
          {"    "}- Fire App Dev {"\n"}
          {"    "}- Pradip Debnath {"\n"}
		  {"    "}- Stack Overflow {"\n"}
          {"    "}- Dll{" "}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  textContent: {
    margin: 10,
    alignItems: "center",
    textAlign: "justify",
  },
});
