import React, { useContext, useState } from "react";
import {
  Text,
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Alert,
  Button,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import LoginScreen from "./LoginScreen";
import { AuthContext } from "../navigator/AuthProvider";

const { width: Width, height: HEIGHT } = Dimensions.get("window");

export default function RegisterScreen({ navigation }) {
  const [showPass, setShowPass] = useState(true);
  const [press, setPress] = useState(false);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // const [fullName, setFullName] = useState("");

  const { register } = useContext(AuthContext);

  const showPass1 = () => {
    if (press == false) {
      setShowPass(!showPass);
      setPress(!press);
    } else {
      setShowPass(!showPass);
      setPress(!press);
    }
  };

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={require("../asset/logo.png")} />
      <Text style={styles.title}>ToDo - App</Text>
      <Text style={styles.subTitle}>
        Mudahkan Harimu, Lancarkan Aktivitasmu
      </Text>

      <View style={styles.contentBox}>
        <Text style={styles.welcomeTitle}>Create an account now !!!</Text>
        <Text style={styles.subWelcomeTitle}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry.
        </Text>

        {/* <View style={styles.userInputContainer}>
          <Feather
            name="user"
            size={28}
            color="gray"
            style={{ position: "absolute", top: 5, left: 25 }}
          />
          <TextInput
            style={styles.userInput}
            placeholder={"Full Name"}
            underlineColorAndroid="transparent"
            value={fullName}
            onChangeText={(value) => setFullName(value)}
          />
        </View> */}

        <View style={styles.userInputContainer}>
          <Feather
            name="user"
            size={28}
            color="gray"
            style={{ position: "absolute", top: 5, left: 25 }}
          />
          <TextInput
            style={styles.userInput}
            placeholder={"Email"}
            underlineColorAndroid="transparent"
            value={email}
            onChangeText={(value) => setEmail(value)}
          />
        </View>

        <View style={styles.userInputContainer}>
          <Feather
            name="lock"
            size={28}
            color="gray"
            style={{ position: "absolute", top: 5, left: 25 }}
          />
          <TextInput
            style={styles.userInput}
            placeholder={"Password"}
            secureTextEntry={showPass}
            underlineColorAndroid="transparent"
            value={password}
            onChangeText={(value) => setPassword(value)}
          />
          <TouchableOpacity
            style={{ position: "absolute", top: 8, left: "85%" }}
            onPress={showPass1}
          >
            <Feather
              name={press === false ? "eye-off" : "eye"}
              size={26}
              color="gray"
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={styles.loginButton}
          onPress={() => register(email, password)}
        >
          <Text style={styles.loginText}>REGISTER</Text>
        </TouchableOpacity>

        <Text style={styles.socialText}>or register with</Text>

        <View style={styles.socialView}>
          <TouchableOpacity
            style={styles.socialButton}
            onPress={() => alert("Under Consturction")}
          >
            <Image
              style={styles.socialImage}
              source={require("../asset/google.png")}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.socialButton}
            onPress={() => alert("Under Consturction")}
          >
            <Image
              style={styles.socialImage}
              source={require("../asset/facebook.png")}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.socialButton}
            onPress={() => alert("Under Consturction")}
          >
            <Image
              style={{ width: 32, height: 26 }}
              source={require("../asset/twitter.png")}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: "row",
            top: 40,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text style={{ textAlign: "center" }}>Have an account? </Text>
          <TouchableOpacity onPress={() => navigation.navigate("LoginScreen")}>
            <Text style={{ color: "#4299FF" }}>Login now</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#4299FF",
  },
  logo: {
    width: 95,
    height: 110,
  },
  title: {
    color: "white",
    fontSize: 32,
    fontWeight: "bold",
  },
  subTitle: {
    color: "white",
    fontSize: 14,
    marginBottom: 20,
  },
  contentBox: {
    top: 30,
    width: Width,
    height: "70%",
    backgroundColor: "white",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
  welcomeTitle: {
    fontSize: 24,
    fontWeight: "bold",
    top: 25,
    left: 20,
  },
  subWelcomeTitle: {
    fontSize: 12,
    top: 30,
    left: 20,
  },
  userInputContainer: {
    top: 40,
    marginTop: 20,
  },
  userInput: {
    width: Width - 45,
    height: 40,
    borderWidth: 2,
    borderColor: "black",
    marginHorizontal: 20,
    paddingLeft: 45,
  },
  loginButton: {
    width: Width - 46,
    height: 40,
    backgroundColor: "#4299FF",
    justifyContent: "center",
    top: 60,
    left: 20,
  },
  loginText: {
    color: "white",
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
  },
  forgotPassText: {
    top: 65,
    left: "65%",
  },
  socialText: {
    top: 80,
    textAlign: "center",
  },
  socialView: {
    marginTop: 90,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  socialButton: {
    width: 40,
    height: 40,
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "black",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 5,
    marginRight: 5,
  },
  socialImage: {
    width: 32,
    height: 32,
  },
});
