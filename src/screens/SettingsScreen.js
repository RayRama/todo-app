import React, { useContext } from "react";
import {
  Alert,
  Button,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { AuthContext } from "../navigator/AuthProvider";
import { Feather, AntDesign } from "@expo/vector-icons/";

export default function SettingsScreen({ navigation }) {
  const { logout } = useContext(AuthContext);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={[styles.textHeader, { fontWeight: "bold", fontSize: 24 }]}>
          SETTINGS
        </Text>
        <Text
          style={[
            styles.textHeader,
            { fontWeight: "bold", fontSize: 14, top: 15 },
          ]}
        >
          “Mula-mula, kau harus merubah dirimu sendiri, atau tidak akan ada yang
          berubah untukmu.” – Sakata Gintoki
        </Text>
      </View>

      <View style={styles.contentSettings}>
        <TouchableOpacity
          style={styles.touchableSetting}
          onPress={() => navigation.navigate("About Us")}
        >
          <Feather name="user" size={30} color="black" />
          <Text style={styles.textSettings}>About Us</Text>
        </TouchableOpacity>

        <View style={styles.divider}></View>

        <TouchableOpacity
          style={styles.touchableSetting}
          onPress={() => navigation.navigate("Info")}
        >
          <Feather name="info" size={30} color="black" />
          <Text style={styles.textSettings}>Info Aplikasi</Text>
        </TouchableOpacity>

        <View style={styles.divider}></View>

        <TouchableOpacity
          style={styles.touchableSetting}
          onPress={() =>
            Alert.alert("Konfirmasi", "Apakah anda ingin logout ?", [
              { text: "No" },
              { text: "Yes", onPress: () => logout() },
            ])
          }
        >
          <AntDesign name="logout" size={30} color="black" />
          <Text style={styles.textSettings}>Logout</Text>
        </TouchableOpacity>
      </View>
      {/* <Button title="Logout" onPress={() => logout()} /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    top: 40,
    height: "40%",
    backgroundColor: "#4299FF",
    justifyContent: "center",
    alignItems: "center",
  },
  textHeader: {
    color: "white",
    bottom: 15,
    textAlign: "center",
  },
  contentSettings: {
    top: 40,
    marginLeft: 15,
  },
  touchableSetting: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 20,
  },
  textSettings: {
    fontSize: 18,
    fontWeight: "bold",
    left: 10,
    color: "black",
  },
  divider: {
    top: 11,
    width: "100%",
    height: StyleSheet.hairlineWidth,
    backgroundColor: "black",
  },
});
