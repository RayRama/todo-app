import React, { Component } from "react";
import {
  Text,
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Animated,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const { width: Width, height: HEIGHT } = Dimensions.get("window");

export default class AboutScreen extends Component<Props> {
  state = {
    active: 0,
    xProject: 0,
    xKontak: 0,
    translateX: new Animated.Value(0),
    translateXProject: new Animated.Value(0),
    translateXKontak: new Animated.Value(Width),
    translateY: -1000,
  };

  handleSlide = (type) => {
    let {
      active,
      xProject,
      xKontak,
      translateX,
      translateXProject,
      translateXKontak,
    } = this.state;
    Animated.spring(translateX, {
      toValue: type,
      duration: 100,
      useNativeDriver: true,
    }).start();

    if (active === 0) {
      Animated.parallel([
        Animated.spring(translateXProject, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true,
        }).start(),
        Animated.spring(translateXKontak, {
          toValue: Width,
          duration: 100,
          useNativeDriver: true,
        }).start(),
      ]);
    } else {
      Animated.parallel([
        Animated.spring(translateXProject, {
          toValue: -Width,
          duration: 100,
          useNativeDriver: true,
        }).start(),
        Animated.spring(translateXKontak, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true,
        }).start(),
      ]);
    }
  };

  render() {
    let {
      active,
      xProject,
      xKontak,
      translateX,
      translateXProject,
      translateXKontak,
      translateY,
    } = this.state;
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../asset/profile.png")} />
        <Text style={styles.title}>Ray Ramadita</Text>
        <Text style={styles.subTitle}>React Native Developer</Text>

        <View style={styles.contentBox}>
          <View
            style={{ width: "90%", marginLeft: "auto", marginRight: "auto" }}
          >
            <View
              style={{
                flexDirection: "row",
                marginTop: 30,
                marginBottom: 30,
                height: 36,
                position: "relative",
              }}
            >
              <Animated.View
                style={{
                  position: "absolute",
                  width: "50%",
                  top: 0,
                  left: 0,
                  backgroundColor: "#4299FF",
                  height: "100%",
                  borderRadius: 4,
                  transform: [{ translateX }],
                }}
              />
              <TouchableOpacity
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  borderWidth: 1,
                  borderRadius: 4,
                  borderColor: "#4299FF",
                  borderRightWidth: 0,
                  borderTopRightRadius: 0,
                  borderBottomRightRadius: 0,
                }}
                onLayout={(event) =>
                  this.setState({ xProject: event.nativeEvent.layout.x })
                }
                onPress={() =>
                  this.setState({ active: 0 }, () => this.handleSlide(xProject))
                }
              >
                <Text style={{ color: active === 0 ? "#fff" : "#4299FF" }}>
                  Portofolio
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  borderWidth: 1,
                  borderRadius: 4,
                  borderColor: "#4299FF",
                  borderLeftWidth: 0,
                  borderTopLeftRadius: 0,
                  borderBottomLeftRadius: 0,
                }}
                onLayout={(event) =>
                  this.setState({ xKontak: event.nativeEvent.layout.x })
                }
                onPress={() =>
                  this.setState({ active: 1 }, () => this.handleSlide(xKontak))
                }
              >
                <Text style={{ color: active === 1 ? "#fff" : "#4299FF" }}>
                  Kontak
                </Text>
              </TouchableOpacity>
            </View>

            <ScrollView>
              <Animated.View
                style={{
                  justifyContent: "flex-start",
                  transform: [{ translateX: translateXProject }],
                }}
                onLayout={(event) =>
                  this.setState({ translateY: event.nativeEvent.layout.height })
                }
              >
                <Text
                  style={{ fontSize: 18, textAlign: "center", lineHeight: 30 }}
                >
                  Beberapa Project yang Pernah Saya Kerjakan
                </Text>
                <View
                  style={{
                    marginTop: 20,
                    justifyContent: "flex-start",
                    alignItems: "flex-start",
                  }}
                >
                  <View
                    style={{
                      margin: 10,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <AntDesign name="github" size={32} />
                    <Text style={{ marginLeft: 10 }}>@RayRama</Text>
                  </View>
                  <View
                    style={{
                      margin: 10,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <AntDesign name="gitlab" size={32} />
                    <Text style={{ marginLeft: 10 }}>@RayRama</Text>
                  </View>
                  <View
                    style={{
                      margin: 10,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <MaterialCommunityIcons name="web-box" size={32} />
                    <Text style={{ marginLeft: 10 }}>
                      https://smanegeri1sindang.sch.id
                    </Text>
                  </View>
                </View>
              </Animated.View>

              <Animated.View
                style={{
                  justifyContent: "flex-start",
                  transform: [
                    { translateX: translateXKontak },
                    { translateY: -translateY },
                  ],
                }}
              >
                <Text
                  style={{ fontSize: 18, textAlign: "center", lineHeight: 30 }}
                >
                  Hubungkan dengan Sosial Media Saya
                </Text>
                <View
                  style={{
                    marginTop: 20,
                    justifyContent: "flex-start",
                    alignItems: "flex-start",
                  }}
                >
                  <View
                    style={{
                      margin: 10,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Ionicons name="mail-open" size={32} />
                    <Text style={{ marginLeft: 10 }}>
                      rayramadita12@gmail.com
                    </Text>
                  </View>
                  <View
                    style={{
                      margin: 10,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <AntDesign name="instagram" size={32} />
                    <Text style={{ marginLeft: 10 }}>@r.rama12</Text>
                  </View>
                  <View
                    style={{
                      margin: 10,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <MaterialCommunityIcons name="whatsapp" size={32} />
                    <Text style={{ marginLeft: 10 }}>08123456789</Text>
                  </View>
                </View>
              </Animated.View>
            </ScrollView>
          </View>

          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <TouchableOpacity
              style={styles.skillButton}
              onPress={() => this.props.navigation.navigate("Skill")}
            >
              <Text style={styles.skillText}>Daftar Skill</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#4299FF",
  },
  logo: {
    width: 105,
    height: 105,
  },
  title: {
    color: "white",
    fontSize: 32,
    fontWeight: "bold",
  },
  subTitle: {
    color: "white",
    fontSize: 14,
    marginBottom: 20,
  },
  contentBox: {
    top: 30,
    width: Width,
    height: "70%",
    backgroundColor: "white",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
  skillButton: {
    width: Width - 115,
    height: 40,
    borderRadius: 7,
    backgroundColor: "#4299FF",
    top: -HEIGHT / 6,
  },
  skillText: {
    color: "white",
    textAlign: "center",
    fontSize: 14,
    fontWeight: "bold",
    top: 10,
  },
});
